import React from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { makeStyles, Grid, Typography, Button } from "@material-ui/core";

import Avatar from "@material-ui/core/Avatar";
import CssBaseline from "@material-ui/core/CssBaseline";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Container from "@material-ui/core/Container";
import {clear} from '../login/login.actions'

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const Profile = props => {
  const classes = useStyles();
  const history = useHistory();

  const handleLogout = e => {
    e.preventDefault();
    localStorage.clear()
    props.onLogout()
    history.replace("/");
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Profile
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Typography variant="h2">
                {`${props.authentication.firstName}`}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography variant="h2">
                {`${props.authentication.lastName}`}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h2">
                {`${props.authentication.emailId}`}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                onClick={handleLogout}
                className={classes.submit}
              >
                Log Out
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

const mapStateToProps = state => ({
  authentication: state.authenticationState.authentication
});

const mapDispatchToProps = dispatch => ({
  onLogout : () => dispatch(clear)
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
