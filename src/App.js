import React from "react";
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import authenticationState from "./login/login.reducers";
import { PersistGate } from "redux-persist/integration/react";

import Login from "./login/login";
import SignIn from "./login/sign-in";
import LastStep from "./otp/otp";
import Profile from "./profile/profile";

// Setup Redux store with Thunks
const reducers = combineReducers({ authenticationState });
const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunk)
  // other store enhancers if any
);

const persistConfig = {
  key: "root",
  storage
};

const persistedReducer = persistReducer(persistConfig, reducers);
const store = createStore(persistedReducer, enhancer);
let persistor = persistStore(store);
const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/sign-in" component={SignIn} />
          <Route path="/last-step" component={LastStep} />
          <Route path="/profile" component={Profile} />
          <Route path="/*" component={() => "NOT FOUND"} />
        </Switch>
      </Router>
    </PersistGate>
  </Provider>
);

export default App;
