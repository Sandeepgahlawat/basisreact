import { Authentication } from "./Authentication";
import { User } from './User'
import { Otp } from './Otp'

export interface AuthenticationRepository {
  Authenticate(userData: User): Promise<Authentication>;
  SignUpUser(userData:User) : Promise<Authentication>;
  VerifyOTP(otp:Otp): Promise<Authentication>;
}
