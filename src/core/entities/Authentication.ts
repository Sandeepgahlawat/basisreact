export class Authentication {
  id: string
  firstName: string;
  lastName: string;
  userName: string;
  emailId: string;
  password: string;
  otpStatus: string;
  createdOn: number;

  constructor( id: string,
    firstName: string,
    lastName: string,
    userName: string,
    emailId: string,
    password: string,
    otpStatus: string,
    createdOn: number
  ) {
    this.id = id
    this.firstName = firstName
    this.lastName = lastName
    this.userName = userName
    this.emailId = emailId
    this.password = password
    this.otpStatus = otpStatus
    this.createdOn = createdOn
  }
}
