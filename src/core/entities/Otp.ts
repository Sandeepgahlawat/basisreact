export class Otp {
  passCode: string;
  referral: string;

  constructor(passCode: string, referral: string) {
    this.passCode = passCode;
    this.referral = referral;
  }
}
