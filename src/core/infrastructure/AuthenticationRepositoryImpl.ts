import { Authentication } from "../entities/Authentication";
import { User } from "../entities/User";
import {Otp} from "../entities/Otp"
import { AuthenticationRepository } from "../entities/AuthenticationRepository";

export class AuthenticationRespositoryImpl implements AuthenticationRepository {
  private static instance: AuthenticationRespositoryImpl;

  private constructor() {}

  public static getInstance(): AuthenticationRespositoryImpl {
    if (!AuthenticationRespositoryImpl.instance) {
      AuthenticationRespositoryImpl.instance = new AuthenticationRespositoryImpl();
    }

    return AuthenticationRespositoryImpl.instance;
  }

  jsonUrl = "http://localhost:8000/sign-in";
  signUpUrl = "http://localhost:8000/sign-up";

  OTP_VERIFICATION_API = "http://localhost:8000/verify-otp";

  async Authenticate(userData: User): Promise<Authentication> {
    console.log("authenticate user :", userData);
    const res = await fetch(this.jsonUrl, {
      method: "POST",
      body: JSON.stringify(userData),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    });
    const jsonData = await res.json();
    console.log("authenticate user :", jsonData);
    const user = jsonData.user;
    localStorage.setItem("AUTH", jsonData.token);
    return new Authentication(
      user.id,
      user.first_name,
      user.last_name,
      user.full_name,
      user.email_id,
      user.password,
      `${user.otp_status}`,
      user.created_on
    );
  }

  async SignUpUser(userData: User): Promise<Authentication> {
    console.log("signing up", userData);
    const res = await fetch(this.signUpUrl, {
      method: "POST",
      body: JSON.stringify(userData),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    });
    const jsonData = await res.json();
    console.log("jsonData :", jsonData);
    const user = jsonData.user;
    localStorage.setItem("AUTH", jsonData.token);
    return new Authentication(
      user.id,
      user.first_name,
      user.last_name,
      user.full_name,
      user.email_id,
      user.password,
      `${user.otp_status}`,
      user.created_on
    );
  }

  async VerifyOTP(otp: Otp): Promise<Authentication> {
    const token = localStorage.getItem("AUTH");
    const res = await fetch(this.OTP_VERIFICATION_API, {
      method: "POST",
      body: JSON.stringify(otp),
      headers: {
        "Content-Type": "application/json",
        Authorization: `${token}`
      },
      credentials: "same-origin"
    });
    const jsonData = await res.json();
    console.log("jsonData :", jsonData);
    const user = jsonData.user;
    return new Authentication(
      user.id,
      user.first_name,
      user.last_name,
      user.full_name,
      user.email_id,
      user.password,
      `${user.otp_status}`,
      user.created_on
    );
  }
}
