import { Authentication } from "../entities/Authentication";
import { AuthenticationRepository } from "../entities/AuthenticationRepository";
import { User } from "../entities/User";
import {Otp} from '../entities/Otp'

export interface AuthenticateService {
  AuthenticateUser(userData: User): Promise<Authentication>;
  SignUpUser(userData: User): Promise<Authentication>;
  VerifyOtp(otp: Otp): Promise<Authentication>;
}

export class AuthenticateServiceImpl implements AuthenticateService {
  private static instance: AuthenticateServiceImpl;

  authRepo: AuthenticationRepository;

  private constructor(ar: AuthenticationRepository) {
    this.authRepo = ar;
  }

  public static getInstance(
    ar: AuthenticationRepository
  ): AuthenticateServiceImpl {
    if (!AuthenticateServiceImpl.instance) {
      AuthenticateServiceImpl.instance = new AuthenticateServiceImpl(ar);
    }

    return AuthenticateServiceImpl.instance;
  }

  async AuthenticateUser(user: User): Promise<Authentication> {
    return this.authRepo.Authenticate(user);
  }

  async SignUpUser(userData: User): Promise<Authentication> {
    return this.authRepo.SignUpUser(userData);
  }

  async VerifyOtp(otp: Otp): Promise<Authentication> {
    return this.authRepo.VerifyOTP(otp);
  }
}
