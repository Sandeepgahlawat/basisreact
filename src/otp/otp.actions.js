import { OTP_VERIFY_INITIATE, OTP_SUCCESS, OTP_FAILURE } from "./otp.types";
import { AuthenticateServiceImpl } from "../core/usecases/AuthenticationService";
import { AuthenticationRespositoryImpl } from "../core/infrastructure/AuthenticationRepositoryImpl";

export const verifyOTP = otp => {
  return async dispatch => {
    dispatch({ type: OTP_VERIFY_INITIATE });

    try {
      const itemRepo = AuthenticationRespositoryImpl.getInstance();
      const itemService = AuthenticateServiceImpl.getInstance(itemRepo);
      const items = await itemService.VerifyOtp(otp);
      dispatch({ type: OTP_SUCCESS, payload: items });
    } catch (error) {
      dispatch({ type: OTP_FAILURE, error });
    }
  };
};
