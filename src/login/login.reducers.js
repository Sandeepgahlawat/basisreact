import {
  LOGIN_INITIATE,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  CLEAR_DATA
} from "./login.types";

import {
  OTP_VERIFY_INITIATE,
  OTP_SUCCESS,
  OTP_FAILURE
} from "../otp/otp.types";

const initialState = {
  loading: false,
  authentication: {},
  userDataBeforeSignUp: {}
};

function authenticationState(state = initialState, action = null) {
  console.log("in reducer:", action);
  switch (action.type) {
    case LOGIN_INITIATE:
      return {
        ...state,
        loading: true
      };

    case LOGIN_FAILURE:
      return {
        ...state,
        loading: false
      };
    case CLEAR_DATA:
      return {
        ...state,
        authentication: {}
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        authentication: action.payload,
        loading: false
      };

    case OTP_VERIFY_INITIATE:
      return {
        ...state,
        loading: true
      };

    case OTP_FAILURE:
      return {
        ...state,
        loading: false
      };

    case OTP_SUCCESS:
      return {
        ...state,
        authentication: action.payload,
        loading: false
      };
    default:
      return state;
  }
}

export default authenticationState;
