import {
  LOGIN_INITIATE,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  CLEAR_DATA
} from "./login.types";
import { AuthenticateServiceImpl } from "../core/usecases/AuthenticationService";
import { AuthenticationRespositoryImpl } from "../core/infrastructure/AuthenticationRepositoryImpl";
import { User } from "../core/entities/User";

export const initiateLogin = user => {
  console.log("inside initiate login", user);
  return async dispatch => {
    dispatch({ type: LOGIN_INITIATE });

    try {
      const itemRepo = AuthenticationRespositoryImpl.getInstance();
      const itemService = AuthenticateServiceImpl.getInstance(itemRepo);
      const userData = new User(
        user.firstName,
        user.lastName,
        user.email,
        user.password
      );
      const items = await itemService.SignUpUser(userData);
      dispatch({ type: LOGIN_SUCCESS, payload: items });
    } catch (error) {
      dispatch({ type: LOGIN_FAILURE, error });
    }
  };
};

export const clear = async dispatch =>{
  dispatch({ type: CLEAR_DATA });
}

export const initiateSignIn = user => {
  console.log("inside initiate signIn", user);
  return async dispatch => {
    dispatch({ type: LOGIN_INITIATE });

    try {
      const itemRepo = AuthenticationRespositoryImpl.getInstance();
      const itemService = AuthenticateServiceImpl.getInstance(itemRepo);
      const userData = new User(
        user.firstName,
        user.lastName,
        user.email,
        user.password
      );
      const items = await itemService.AuthenticateUser(userData);
      dispatch({ type: LOGIN_SUCCESS, payload: items });
    } catch (error) {
      dispatch({ type: LOGIN_FAILURE, error });
    }
  };
};


