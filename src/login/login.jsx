import React from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  makeStyles,
  Grid,
  TextField,
  Typography,
  Button,
  Backdrop,
  CircularProgress
} from "@material-ui/core";
// import { refreshList } from "./Item.actions";
import { initiateLogin } from "./login.actions";

import Avatar from "@material-ui/core/Avatar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Container from "@material-ui/core/Container";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const Login = props => {
  const classes = useStyles();
  const history = useHistory();

  const [open, setOpen] = React.useState(false);
  const [firstName, setFirstName] = React.useState("");
  const [lastName, setLastName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");

  React.useEffect(() => {
    console.log("printing authentication effect: ", props.authentication);
    if (
      props.authentication.hasOwnProperty("otpStatus") &&
      props.authentication.otpStatus === "0"
    ) {
      history.push("/last-step");
    } else if (
      props.authentication.hasOwnProperty("otpStatus") &&
      props.authentication.otpStatus === "1"
    ) {
      history.push("/profile");
    }
    setOpen(props.loading)
  }, [props.authentication]);



  const handleFirstNameChange = event => {
    event.preventDefault();
    console.log(event.target.value);
    setFirstName(event.target.value);
  };

  const handleLastNameChange = event => {
    event.preventDefault();
    console.log(event.target.value);
    setLastName(event.target.value);
  };

  const handleEmailChange = event => {
    event.preventDefault();
    console.log(event.target.value);
    setEmail(event.target.value);
  };

  const handlePasswordChange = event => {
    event.preventDefault();
    console.log(event.target.value);
    setPassword(event.target.value);
  };

  const handleSignIn = event => {
    event.preventDefault();
    console.log("inside btn click");
    props.onSignUp({
      firstName,
      lastName,
      email,
      password
    });
  };

  const openSignInPage = e => {
    e.preventDefault();
    history.push("/sign-in");
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="firstName"
                  variant="outlined"
                  required
                  fullWidth
                  onChange={handleFirstNameChange}
                  id="firstName"
                  label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  onChange={handleLastNameChange}
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="lname"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  onChange={handleEmailChange}
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  onChange={handlePasswordChange}
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              onClick={handleSignIn}
              className={classes.submit}
            >
              Sign Up
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href="#" variant="body2" onClick={openSignInPage}>
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Container>
      <Backdrop className={classes.backdrop} open={props.loading} >
        <CircularProgress color="inherit" />
      </Backdrop>
    </React.Fragment>
  );
};

const mapStateToProps = state => ({
  authentication: state.authenticationState.authentication,
  loading: state.authenticationState.loading
});

const mapDispatchToProps = dispatch => {
  return {
    onSignUp: user => dispatch(initiateLogin(user))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
